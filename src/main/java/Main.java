import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;
import spark.*;
import spark.Request;
import spark.Response;
import spark.template.freemarker.FreeMarkerEngine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static final String PROTECTED_RESOURCE_URL = "https://api.twitter.com/1.1/account/verify_credentials.json";
    private static OAuthService service;

    public static void main(String[] args) {
        //get("/hello", (req, res) -> "Hello World");

        Spark.staticFileLocation("/public");

        Spark.get("/hello", new Route() {
            @Override
            public Object handle(Request request, Response response) throws Exception {
                return "Hello World";
            }
        });

        Spark.get("/free", new TemplateViewRoute() {
            @Override
            public ModelAndView handle(Request request, Response response) {
                Map<String, Object> attributes = new HashMap<>();
                attributes.put("message", "Hello World!");

                //List parsing
                List<String> countries = new ArrayList<String>();
                countries.add("India");
                countries.add("United States");
                countries.add("Germany");
                countries.add("France");

                attributes.put("countries", countries);

                return new ModelAndView(attributes, "free.ftl");
            }
        }, new FreeMarkerEngine());

        Spark.get("/twitter", new TemplateViewRoute() {
            @Override
            public ModelAndView handle(Request request, Response response) {
                return new ModelAndView(null, "twitter.ftl");
            }
        }, new FreeMarkerEngine());

        Spark.post("/twitter/tweets", new TemplateViewRoute() {
            @Override
            public ModelAndView handle(Request request, Response response) {
                String licenseCode = request.queryParams("licenseCode");
                print(licenseCode);

                Map<String, Object> attributes = new HashMap<>();
                attributes.put("licenseCode", licenseCode);

                File f = new File("./" + licenseCode);
                if(f != null && f.exists() == false) {
                    return new ModelAndView(attributes, "init-oauth.ftl");
                } else {
                    return new ModelAndView(attributes, "tweets.ftl");
                }
            }
        }, new FreeMarkerEngine());

        Spark.post("/twitter/oauth/keys", new TemplateViewRoute() {
            @Override
            public ModelAndView handle(Request request, Response response) {
                String apiKey = request.queryParams("apiKey");
                String apiSecret = request.queryParams("apiSecret");
                String licenseCode = request.queryParams("licenseCode");
                print(licenseCode + ", " + apiKey + ", " + apiSecret);

                initOauth(response, apiKey, apiSecret, licenseCode);

                return null;
            }
        }, new FreeMarkerEngine());

        Spark.get("/twitter/oauthcallback", new TemplateViewRoute() {
            @Override
            public ModelAndView handle(Request request, Response response) {
                String oauthToken = request.queryParams("oauth_token");
                String oauthVerifier = request.queryParams("oauth_verifier");

                print(oauthToken + ", " + oauthVerifier);

                Token requestToken = new Token(oauthToken, oauthVerifier);
                Verifier verifier = new Verifier(oauthVerifier);

                Token accessToken = service.getAccessToken(requestToken, verifier);
                print("accessToken = " + accessToken.getRawResponse());
                OAuthRequest oauthRequest = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
                service.signRequest(accessToken, oauthRequest);

                org.scribe.model.Response oauthRequestResponse = oauthRequest.send();
                System.out.println("Got it! Lets see what we found...");
                System.out.println();
                System.out.println(oauthRequestResponse.getBody());

                return new ModelAndView(null, "oauth-done.ftl");
            }
        }, new FreeMarkerEngine());
    }

    private static boolean shouldReturnHtml(Request request) {
        String accept = request.headers("Accept");
        return accept != null && accept.contains("text/html");
    }

    private static void print(String msg) {
        System.out.println(msg);
    }

    private static void initOauth(Response response, String apiKey, String apiSecret, String licenseCode){
        service = new ServiceBuilder()
                .provider(TwitterApi.class)
                .apiKey(apiKey)
                .apiSecret(apiSecret)
                .callback("http://localhost:4567/twitter/oauthcallback")
                .build();

        // Obtain the Request Token
        System.out.println("Fetching the Request Token...");
        Token requestToken = service.getRequestToken();
        System.out.println("Got the Request Token!");
        System.out.println();

        System.out.println("Now go and authorize Scribe here:");

        String redirectUrl = service.getAuthorizationUrl(requestToken);
        System.out.println(redirectUrl);
        response.redirect(redirectUrl);
    }

    private static void saveTokenTofile(Token token, String fileName) throws Exception {
        FileOutputStream fout = new FileOutputStream("./" + fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(token);oos.flush();
        oos.close();
        fout.close();

    }
}
